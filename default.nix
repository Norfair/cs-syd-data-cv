{ pkgs ? import (import ./nixpkgs.nix) { config.allowUnfree = true; }
}:

with pkgs;

let
  cvgen = import ./cvgen.nix { inherit pkgs; };
  gitignoreSource = import ./gitignore.nix { inherit pkgs; };
  latexenv = import ./latexenv.nix { inherit pkgs; };

in stdenv.mkDerivation {
  name = "cv";
  buildInputs = [
    bash
    cvgen
    latexenv
  ];
  src = gitignoreSource ./.;
  phases = "unpackPhase buildPhase";
  buildPhase = ''
    mkdir -p $out
    cvgen
    cd auto/gen
    cp timeline.html $out/
    latexmk -pdf failcv.tex
    cp failcv.pdf $out/
    latexmk -pdf for-recruiters.tex
    cp for-recruiters.pdf $out/
    latexmk -pdf for-engineers.tex
    cp for-engineers.pdf $out/
    latexmk -pdf cv.tex
    cp cv.pdf $out/
  '';
}

